package com.bluetooth.bth_k2


import android.Manifest
import android.annotation.SuppressLint
import android.bluetooth.*
import android.bluetooth.le.ScanCallback
import android.bluetooth.le.ScanResult
import android.content.Context
import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import java.io.File
import java.io.FileOutputStream
import java.time.Instant
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap




class MainActivity : AppCompatActivity() {

    val instance by lazy { this }
    var bHeadset: BluetoothHeadset? = null
    private val REQUEST_CODE_ENABLE_BT: Int = 1;
    var ctx :Context?
        get() {
            TODO()
        }
        set(value) {}


    var Conn_Stage = false
    /// f:disconnected  t:connected

    //lateinit var bManager : BluetoothManager
    var bAdapter : BluetoothAdapter? = null
    lateinit var pairedDevice : Set<BluetoothDevice>
    val REQUEST_ENABLE_BLUETOOTH = 1




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        bAdapter = BluetoothAdapter.getDefaultAdapter()

    }

    private val profileListener = object : BluetoothProfile.ServiceListener {

        override fun onServiceConnected(profile: Int, proxy: BluetoothProfile) {
            if (profile == BluetoothProfile.HEADSET) {
                bHeadset = proxy as BluetoothHeadset
            }
        }

        override fun onServiceDisconnected(profile: Int) {
            if (profile == BluetoothProfile.HEADSET) {
                bHeadset = null
            }
        }
    }



    fun clickConn(view: View) {
        if(!Conn_Stage) {
            if (bAdapter?.isEnabled == false) {
                val intent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
                startActivityForResult(intent, REQUEST_CODE_ENABLE_BT)
            }
            bAdapter?.getProfileProxy(instance , profileListener, BluetoothProfile.HEADSET)

            Conn_Stage = true

        }
        else{
            Log.e("BTH_CON","already connected")
        }
    }





    fun clickDiscon(view: View) {

        bAdapter!!.disable()
        if(Conn_Stage){
            bAdapter?.closeProfileProxy(BluetoothProfile.HEADSET, bHeadset)
                bHeadset = null
                Conn_Stage = false
        }
        else{


        }
    }
    fun clickDatabase(view: View) {

    }


}